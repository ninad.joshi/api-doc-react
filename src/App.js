import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles'; // For optional styling

const useStyles = makeStyles((theme) => ({
  //Define styles for various elements (optional)
  apiDocContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2),
  },
  apiEndpoint: {
    fontWeight: 'bold',
    marginBottom: theme.spacing(1),
  },
  apiDescription: {
    marginBottom: theme.spacing(2),
  },
  parameterList: {
    marginLeft: theme.spacing(2),
  },
  parameter: {
    marginBottom: theme.spacing(0.5),
  },
}));

const App = () => {
  const classes = useStyles(); // Get styles from makeStyles
  const [apiData, setApiData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const buildCurlCommand = (method, url, requestBody) => {
    let curlCommand = `curl`;
    curlCommand += ` -X ${method.toUpperCase()}`; // Set HTTP method
    curlCommand += ` ${url}`; // Add base URL and endpoint

    // Add headers if applicable (consider adding a configuration for default headers)
    // curlCommand += ` -H "Content-Type: application/json"`; // Example header

    if (requestBody) {
      // Handle JSON request body
      curlCommand += ` -H "Content-Type: application/json"`;
      curlCommand += ` -d '${JSON.stringify(requestBody)}'`; // Add request body as JSON data
    }

    return curlCommand;
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = (e) => {
      try {
        setIsLoading(true);
        const data = JSON.parse(e.target.result);
        setApiData(data);
      } catch (error) {
        setError(error.message);
      } finally {
        setIsLoading(false);
      }
    };
  };

  useEffect(() => {
    // Handle potential initial data loading if needed
  }, []); // Empty dependency array for effect to run only once

  if (isLoading) {
    return <div>Loading API data...</div>;
  }

  if (error) {
    return <div>Error parsing OpenAPI JSON: {error}</div>;
  }

  if (!apiData) {
    return (
      <div>
        <label htmlFor="apiDocFile">
          Upload OpenAPI JSON file:
          <input type="file" id="apiDocFile" accept=".json" onChange={handleFileChange} />
        </label>
      </div>
    );
  }
  const getschemDetails = (schem) => {
    if (!schem) {
      return 'No schem information available';
    }

    // Check for different schem object structures (might vary depending on OpenAPI version)
    if (schem.properties) {
      const properties = Object.entries(schem.properties).map(([key, value]) => ({
        name: key,
        ...value, // Include relevant properties like type, description, etc.
      }));
      console.log(properties);
      return (
        <ul>
          {properties.map((prop) => (
            <li key={prop.name}>
              {prop.name}: {prop.type} - {prop.description} 
            </li>
          ))}
        </ul>
      );
    } else if (schem.$ref) {
      // Handle references to other schems within the OpenAPI document (if applicable)
      return <div>Referencing schem: {schem.$ref}</div>;
    } else {
      return <div>schem details not supported in this format</div>;
    }
  };

  return (
    <div className={classes.apiDocContainer}>
      {/* {console.log(apiData)} */}
      {Object.entries(apiData.paths).map(([endpoint, methods]) => (
        <div key={endpoint}>
          <h3 className={classes.apiEndpoint}>{endpoint}</h3>
          {Object.entries(methods).map(([method, details]) => (
            <div key={`${endpoint}-${method}`}>
              <p>
                <strong>{method.toUpperCase()}</strong>: {details.summary}
              </p>
              {details.parameters && (
                <div className={classes.parameterList}>
                  <h4>Parameters:</h4>
                  {/* {JSON.stringify(details.parameters[0].schema)} */}
                  {/* <p>{details.parameters[0].name}</p> */}
                  <ul>
                    {details.parameters.map((param) => (
                      <li key={param.name} className={classes.parameter}>
                        {param.name} ({param.in}): {param.description} {param.schema.type}
                      </li>
                    ))}
                  </ul>
                  
                </div>
              )}
              {details.requestBody && (
              <div>
                <h4>Request Body:</h4>
                {getschemDetails(details.requestBody.content?.['application/json']?.schem)}
                <h4>CURL Command:</h4>
            <pre>
              {buildCurlCommand(
                methods,
                `${apiData.basePath}${endpoint}`,
                details.requestBody?.content?.['application/json']?.schem // Extract request body schem (if available)
              )}
            </pre>
              </div>
              
            )}
            {details.responses && (
              <div>
                <h4>Responses:</h4>
                {Object.entries(details.responses).map(([statusCode, response]) => (
                  <div key={statusCode}>
                    <p>
                      <strong>{statusCode}</strong>: {response.description}
                    </p>
                    {response.content && (
                      <div>
                        <p>Response Body:</p>
                        {getschemDetails(response.content?.['application/json']?.schem)}
                      </div>
                    )}
                  </div>
                ))}
              </div>
            )}
            </div>
            
          ))}
          
        </div>
      ))}
    </div>
  );
};

export default App;